import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  comments: any = [];
  formFields: any = {};
  hasError: boolean = false;

  ngOnInit() {

  }

  /**
   * Submit
   */
  submit(){
    if(this.formFields.comment){
      this.comments.push(this.formFields.comment);
      this.formFields.comment = null;
      this.hasError = false;
    }else{
      this.hasError = true;
    }
  }

  /**
   * Index
   * @param index 
   */
  removeItem(index){
    this.comments.splice(index,1);
  }

  title = 'app';
}
